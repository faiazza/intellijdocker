#use this and it will redownload dependencies everytime
#FROM maven:3.6.1-jdk-8 AS buildStage
#To use this you must first run a the Dockerfile-maven file to create the intellijdockerdemomavencache docker image
FROM intellijdockerdemomavencache AS buildStage
COPY . /project
RUN  cd /project && mvn package

FROM openjdk:8
COPY --from=buildStage /project/target/demo-0.0.1-SNAPSHOT.jar /project/target/demo-0.0.1-SNAPSHOT.jar

#run the spring boot application
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-Dblabla", "-jar","/project/target/demo-0.0.1-SNAPSHOT.jar"]


